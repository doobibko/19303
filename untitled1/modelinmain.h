#ifndef MODELINMAIN_H
#define MODELINMAIN_H

#include <QDialog>
#include <QString>
#include <QLabel>
#include <QPixmap>

namespace Ui {
class modelinmain;
}

class modelinmain : public QDialog
{
    Q_OBJECT

public:
    explicit modelinmain(QWidget *parent = nullptr);
    ~modelinmain();
    void ReadJpg(QString& str, QLabel* label);

private slots:


private:
    Ui::modelinmain *ui;
};

#endif // MODELINMAIN_H
