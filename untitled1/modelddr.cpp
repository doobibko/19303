#include "modelddr.h"
#include "ui_modelddr.h"


ModelDDR::ModelDDR(QWidget *parent) :
    QGroupBox(parent),
    ui(new Ui::ModelDDR)
{
    ui->setupUi(this);
    on_checkBox_3_stateChanged(1);
    ui->nextstep->setEnabled(false);
    IdntfLB();
}

ModelDDR::~ModelDDR()
{
    delete ui;
}

void ModelDDR::IdntfLB(){
    QMap<QPair<int, int>, QLabel*> l;
    l[{1, 1}] = ui->one_1;
    l[{1, 2}] = ui->one_2;
    l[{1, 3}] = ui->one_3;
    l[{1, 4}] = ui->one_4;
    l[{1, 5}] = ui->one_5;

    l[{2, 1}] = ui->two_1;
    l[{2, 2}] = ui->two_2;
    l[{2, 3}] = ui->two_3;
    l[{2, 4}] = ui->two_4;
    l[{2, 5}] = ui->two_5;

    l[{3, 1}] = ui->three_1;
    l[{3, 2}] = ui->three_2;
    l[{3, 3}] = ui->three_3;
    l[{3, 4}] = ui->three_4;
    l[{3, 5}] = ui->three_5;

    l[{4, 1}] = ui->four_1;
    l[{4, 2}] = ui->four_2;
    l[{4, 3}] = ui->four_3;
    l[{4, 4}] = ui->four_4;
    l[{4, 5}] = ui->four_5;

    l[{5, 1}] = ui->five_1;
    l[{5, 2}] = ui->five_2;
    l[{5, 3}] = ui->five_3;
    l[{5, 4}] = ui->five_4;
    l[{5, 5}] = ui->five_5;
    lbs = l;
    free(&l);
}

void ModelDDR::ReadPng(QString &str){
    if (str != ""){
        QPixmap pix(str);
        int w = ui->image->width();
        int h = ui->image->height();

        ui->image->clear();
        ui->image->setPixmap(pix.scaled(w, h, Qt::KeepAspectRatio));
    }
}

QPair<int, int> ModelDDR::GetCoords(){
    return (QPair<int, int>{row, column});
}

void ModelDDR:: InsertCoords(const int x, const int y){
    row = x;
    column = y;
}




void ModelDDR::on_sendadress_clicked()
{
    InsertCoords(ui->nrow->currentRow() + 1, ui->ncolumn->currentRow() + 1);
    ui->logs->setText("На буфер адресов был послан адрес ячейки с номером строки = " +
                  QString::number(GetCoords().first) +
                  " и номером столбца = " +
                  QString::number(GetCoords().second));
    ui->sendadress->setEnabled(false);
    ui->nextstep->setEnabled(true);
}

void ModelDDR::on_nextstep_clicked()
{
    on_checkBox_3_stateChanged(1);
    static QString im = "";
    static int count_clicks = 0;
    if (count_clicks == 0){
        ui->logs->setText("Открывается проход");
        im = ":/img/img/adress_memory_a.png";
        ++count_clicks;
    }
    else if (count_clicks == 1){
        ui->logs->setText("Открывается другой проход");
        im = ":/img/img/adress_memory_rowcolumn.png";
        ++count_clicks;
    }
    else if (count_clicks == 2){
        ui->logs->setText("Находим элемент");
        if (ui->checkBox_3){
            lbs[GetCoords()]->setStyleSheet("background-color: #1CD312");
        }
        else
            lbs[GetCoords()]->setStyleSheet("background-color: #E1ED00");
        ++count_clicks;
    }
    else{
        count_clicks = 0;
        im = "";
        ui->sendadress->setEnabled(true);
        ui->nextstep->setEnabled(false);
        lbs[GetCoords()]->setStyleSheet("background-color: black");
    }
    ReadPng(im);

}

void ModelDDR::on_checkBox_3_stateChanged(int arg1)
{
    QString a;
    arg1++;
    if (ui->checkBox_3->isChecked()){
        a = ":/img/img/adress_memory_we.png";
    }else{
        a = ":/img/img/adress_memory_read.png";
    }
    ReadPng(a);
}

