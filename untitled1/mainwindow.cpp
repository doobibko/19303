#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_theory_triggered()
{
    win = new theory(this);
    win->show();
}

void MainWindow::on_adress_triggered()
{
    winddr = new ModelDDR(this);
    winddr->show();
}
