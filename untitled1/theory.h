#ifndef THEORY_H
#define THEORY_H

#include <QDialog>
#include <QListWidget>
#include <QFile>
#include <string>
#include <QTreeWidgetItem>
#include <QMessageBox>
#include <QTextStream>
#include <QTextCodec>
#include <QPixmap>
#include <QApplication>

namespace Ui {
class theory;
}

class theory : public QDialog
{
    Q_OBJECT

public:
    explicit theory(QWidget *parent = nullptr);
    ~theory();

private slots:

    void on_treeWidget_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous);
    void ReadSomeFile(QString& str);
    void ReadSomeJmg(QString& str);

private:
    Ui::theory *ui;
};

#endif // THEORY_H
