#include "theory.h"
#include "ui_theory.h"

theory::theory(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::theory)
{
    ui->setupUi(this);
    ui->treeWidget->children();
}

theory::~theory()
{
    delete ui;
}


void theory::ReadSomeFile(QString& str){
    QFile file(str);
    if ((file.exists()) && (file.open(QIODevice::ReadOnly))){
        QTextStream out(&file);
        out.setCodec("Windows-1251");
        ui->textBrowser->setText(out.readAll());
        file.flush();
        file.close();
    }
}

void theory::ReadSomeJmg(QString& str){
    QPixmap pix(str);
    int w = ui->image->width();
    int h = ui->image->height();

    ui->image->clear();
    ui->image->setPixmap(pix.scaled(w, h, Qt::KeepAspectRatio));
}

void theory::on_treeWidget_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous)
{
    QString file_name = current->text(0);
    QString str, img;
    ui->image->clear();
    if (file_name == "Устройство и организация оперативной памяти"){
        str = ":/text/texts/ddr41.txt";
        img = ":/img/img/1.jpg";
    }else if (file_name == "Команды доступа к памяти"){
        str = ":/text/texts/ddrcomands.txt";
        img = ":/img/img/2.jpg";
    }else if (file_name == "Пропускная способность"){
        str = ":/text/texts/propusksposob.txt";
        img = ":/img/img/ras.jpg";
    }else if (file_name == "Тайминги памяти"){
        str = ":/text/texts/timings.txt";
        img = ":/img/img/cas.jpg";
    }else if (file_name == "Соотношения между таймингами памяти"){
        str = ":/text/texts/sootnosheniya.txt";
        img = ":/img/img/delay.jpg";
    }else if (file_name == "Запись таймингов памяти"){
        str = ":/text/texts/records timings.txt";
        img = ":/img/img/command rate.jpg";
    }else if (file_name == "Появление DDR"){
        str = ":/text/texts/ddr.txt";
    }else if (file_name == "Память DDR2 и DDR3"){
        str = ":/text/texts/ddr2 ddr3.txt";
        img = ":/img/img/ddr123.jpg";
    }else if (file_name == "Работа DDR4"){
        str = ":/text/texts/working ddr4.txt";
        img = ":/img/img/ddr41.jpg";
    }else if (file_name == "Устройство"){
        str = ":/text/texts/KESH.txt";
    }else if (file_name == "Типы КЭШ памяти"){
        str = ":/text/texts/typesKESH.txt";
    }else if (file_name == "Организация внутренней кэш-памяти микропроцессора"){
        str = ":/text/texts/organizeKESH.txt";
    }else if (file_name == "Управление работой кэш-памяти на уровне страниц"){
        str = ":/text/texts/upravl.txt";
    }
    ReadSomeJmg(img);
    ReadSomeFile(str);


}
