#include "modelinmain.h"
#include "ui_modelinmain.h"
#include <QVector>
#include <QMessageBox>

modelinmain::modelinmain(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::modelinmain)
{
    ui->setupUi(this);

}

void modelinmain::ReadJpg(QString& str, QLabel* image){
    QPixmap pix(str);
    int w = image->width();
    int h = image->height();

    image->clear();
    image->setPixmap(pix.scaled(w, h, Qt::KeepAspectRatio));
}

modelinmain::~modelinmain()
{
    delete ui;
}


