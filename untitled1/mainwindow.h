#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "theory.h"
#include "modelddr.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_theory_triggered();

    void on_adress_triggered();

private:
    Ui::MainWindow *ui;
    theory  *win;
    ModelDDR *winddr;
};
#endif // MAINWINDOW_H
