#ifndef MODELDDR_H
#define MODELDDR_H

#include <QGroupBox>
#include <QPixmap>
#include <QPair>
#include <QString>
#include <QMap>
#include <QVector>
#include <QLabel>

namespace Ui {
class ModelDDR;
}

class ModelDDR : public QGroupBox
{
    Q_OBJECT

public:
    explicit ModelDDR(QWidget *parent = nullptr);
    ~ModelDDR();

    void InsertCoords(const int x, const int y);
    QPair<int, int> GetCoords();

    void ReadPng(QString& str);
    void IdntfLB();


private slots:
    void on_sendadress_clicked();

    void on_nextstep_clicked();

    void on_checkBox_3_stateChanged(int arg1);


private:
    Ui::ModelDDR *ui;
    int row;
    int column;
    QMap<QPair<int, int>, QLabel*> lbs;
};

#endif // MODELDDR_H
